import pytest

from zlodziej_crawler import paths
from zlodziej_crawler.cache_helper import cache_get


class MockResponse:
    def __init__(self, content: bytes):
        self.content = content


@pytest.fixture(autouse=True)
def clean_up():
    file_path = paths.CWD_PATH / "sources" / "example.url.com"
    if file_path.is_file():
        file_path.unlink()


def test_cache_helper(mocker):
    mocked_get = mocker.patch("requests.get", return_value=MockResponse(b"testtest"))
    url = "http://example.url.com"

    assert cache_get(url) == b"testtest"
    cache_get(url)
    mocked_get.assert_called_once()
