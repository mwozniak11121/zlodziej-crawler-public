import pytest

from zlodziej_crawler.olx.crawler import OLXCrawler


@pytest.fixture()
def provide_test_data():
    city = "WROCLAW"
    crawler = OLXCrawler(city)

    return city, crawler


@pytest.mark.parametrize(
    "url, expected",
    [
        ("http://olx.pl/offers/123.html", "http://olx.pl/offers/123.html"),
        ("http://olx.pl/offers/123.html#promoted", "http://olx.pl/offers/123.html"),
        ("http://olx.pl/offers/123.html#3098198A2", "http://olx.pl/offers/123.html"),
        (
            "http://otodom.pl/offers/123.html#3098198A2",
            "http://otodom.pl/offers/123.html",
        ),
    ],
)
def test_clean_up_url(url, expected):
    assert OLXCrawler._clean_up_url(url) == expected
