import pytest

from zlodziej_crawler.olx.iterator import OLXIterator


@pytest.fixture()
def provide_test_data():
    category_url = "nieruchomosci/mieszkania/wynajem/wroclaw"
    iterator = OLXIterator(category_url)

    return iterator, category_url


def test_init(provide_test_data):
    iterator, category_url = provide_test_data

    assert (
        iterator._base_url
        == "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/?page={page}"
    )
    assert (
        iterator.first_page
        == "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/"
    )


class MockResponse:
    def __init__(self, status_code: int):
        self.status_code = status_code


def test_validate_url(provide_test_data, mocker):
    iterator, category_url = provide_test_data

    mocker.patch("requests.head", return_value=MockResponse(404))
    with pytest.raises(ValueError):
        iterator._validate_url()

    mocker.patch("requests.head", return_value=MockResponse(200))
    iterator._validate_url()


def test_get_all_pages_urls(provide_test_data, mocker):
    iterator, category_url = provide_test_data

    status_codes = [200, 200, 200, 201, 200, 404]
    responses = [MockResponse(status_code) for status_code in status_codes]
    mocker.patch("requests.head", side_effect=responses)

    expected = [
        "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/",
        "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/?page=2",
        "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/?page=3",
        "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/?page=4",
        "https://www.olx.pl/nieruchomosci/mieszkania/wynajem/wroclaw/?page=5",
    ]
    assert iterator.get_all_pages_urls() == expected
