import datetime
from pathlib import Path

import pytest
from pydantic import HttpUrl

from zlodziej_crawler.models import BuildingType, OfferType, Website
from zlodziej_crawler.olx.parser_factory import OLXParsersFactory
from zlodziej_crawler.utilities import create_soup_from_url


@pytest.fixture()
def provide_test_data():
    parser = OLXParsersFactory.get_parser("nieruchomosci/mieszkania/wynajem/wroclaw")
    url = "http://website.com/test_rent_offer.html"

    return parser, url


def test_scrap_data_from_soup(mocker, provide_test_data):
    cwd_path = Path(__file__).parent
    mocker.patch("zlodziej_crawler.paths.CWD_PATH", cwd_path)

    parser, url = provide_test_data

    result = parser.scrap_data_from_soup(create_soup_from_url(url))
    expected = {
        "cena": "2 000 zl",
        "czas dodania": "dodane\t\t\t\t\t\t\t\t\t\to 17:47, 7 października 2020",
        "czynsz (dodatkowo)": "300 zł",
        "id ogloszenia": "id ogłoszenia: 627115408",
        "liczba pokoi": "2 pokoje",
        "lokacja": "wroclaw, dolnoslaskie, srodmiescie",
        "nazwa oferty": "zamieszkaj w srodku rynku",
        "oferta od": "osoby prywatnej",
        "opis": "do wynajęcia bardzo ładne, klimatyczne mieszkanie położone na płycie "
        "rynku pod ratuszem. 2 pokoje (przejściowe), kuchnia, łazienka z "
        "toaletą i przedpokój. pokoje położone są od podwórka z widokiem na "
        "przejście garnacarskie, kuchnia z widokiem na rynek. umeblowane .do "
        "zamieszkania od października . do czynszu za wynajem dochodzi opłata "
        "za media ok. 300zł. + kaucja 2000,00zł polecam i zapraszam do "
        "kontaktu.",
        "powierzchnia": "45 m²",
        "poziom": "3",
        "rodzaj zabudowy": "kamienica",
        "umeblowane": "tak",
        "wyswietlenia": "wyświetleń:936",
    }

    assert expected == result


def test_create_offer(provide_test_data):
    parser, url = provide_test_data

    scraped_data = {
        "cena": "2 000 zl",
        "czas dodania": "dodane\t\t\t\t\t\t\t\t\t\to 17:47, 7 października 2020",
        "czynsz (dodatkowo)": "300 zł",
        "id ogloszenia": "id ogłoszenia: 627115408",
        "liczba pokoi": "2 pokoje",
        "lokacja": "wroclaw, dolnoslaskie, srodmiescie",
        "nazwa oferty": "zamieszkaj w srodku rynku",
        "oferta od": "osoby prywatnej",
        "opis": "do wynajęcia bardzo ładne, klimatyczne mieszkanie położone na płycie "
        "rynku pod ratuszem. 2 pokoje (przejściowe), kuchnia, łazienka z "
        "toaletą i przedpokój. pokoje położone są od podwórka z widokiem na "
        "przejście garnacarskie, kuchnia z widokiem na rynek. umeblowane .do "
        "zamieszkania od października . do czynszu za wynajem dochodzi opłata "
        "za media ok. 300zł. + kaucja 2000,00zł polecam i zapraszam do "
        "kontaktu.",
        "powierzchnia": "45 m²",
        "poziom": "3",
        "rodzaj zabudowy": "kamienica",
        "umeblowane": "tak",
        "wyswietlenia": "wyświetleń:936",
        "strona": "olx.pl",
        "url": "http://website.com/test_rent_offer.html",
    }

    expected = {
        "area": 45.0,
        "building_type": BuildingType.KAMIENICA,
        "description": "do wynajęcia bardzo ładne, klimatyczne mieszkanie położone na "
        "płycie rynku pod ratuszem. 2 pokoje (przejściowe), kuchnia, "
        "łazienka z toaletą i przedpokój. pokoje położone są od "
        "podwórka z widokiem na przejście garnacarskie, kuchnia z "
        "widokiem na rynek. umeblowane .do zamieszkania od "
        "października . do czynszu za wynajem dochodzi opłata za media "
        "ok. 300zł. + kaucja 2000,00zł polecam i zapraszam do "
        "kontaktu.",
        "floor": "3",
        "furnished": True,
        "id": 627115408,
        "location": "wroclaw, dolnoslaskie, srodmiescie",
        "number_of_rooms": "2",
        "offer_name": "zamieszkaj w srodku rynku",
        "offer_type": OfferType.PRIVATE,
        "price": 2000,
        "price_per_m": 44.44,
        "rent": 300,
        "time_offer_added": datetime.datetime(2020, 10, 7, 17, 47),
        "total_price": 2300,
        "total_price_per_m": 51.11,
        "unused_data": {},
        "url": HttpUrl(
            "http://website.com/test_rent_offer.html",
            scheme="http",
            host="website.com",
            tld="com",
            host_type="domain",
            path="/test_rent_offer.html",
        ),
        "views": 936,
        "website": Website.OLX,
    }

    assert expected == parser.create_offer(scraped_data).dict()


def test_process_url(mocker, provide_test_data):
    cwd_path = Path(__file__).parent
    mocker.patch("zlodziej_crawler.paths.CWD_PATH", cwd_path)

    parser, url = provide_test_data

    expected = {
        "area": 45.0,
        "building_type": BuildingType.KAMIENICA,
        "description": "do wynajęcia bardzo ładne, klimatyczne mieszkanie położone na "
        "płycie rynku pod ratuszem. 2 pokoje (przejściowe), kuchnia, "
        "łazienka z toaletą i przedpokój. pokoje położone są od "
        "podwórka z widokiem na przejście garnacarskie, kuchnia z "
        "widokiem na rynek. umeblowane .do zamieszkania od "
        "października . do czynszu za wynajem dochodzi opłata za media "
        "ok. 300zł. + kaucja 2000,00zł polecam i zapraszam do "
        "kontaktu.",
        "floor": "3",
        "furnished": True,
        "id": 627115408,
        "location": "wroclaw, dolnoslaskie, srodmiescie",
        "number_of_rooms": "2",
        "offer_name": "zamieszkaj w srodku rynku",
        "offer_type": OfferType.PRIVATE,
        "price": 2000,
        "price_per_m": 44.44,
        "rent": 300,
        "time_offer_added": datetime.datetime(2020, 10, 7, 17, 47),
        "total_price": 2300,
        "total_price_per_m": 51.11,
        "unused_data": {"miasto": "WROCław"},
        "url": HttpUrl(
            "http://website.com/test_rent_offer.html",
            scheme="http",
            host="website.com",
            tld="com",
            host_type="domain",
            path="/test_rent_offer.html",
        ),
        "views": 936,
        "website": Website.OLX,
    }

    additional_data = {"strona": "olx.pl", "miasto": "WROCław"}
    assert expected == parser.process_url(url, additional_data).dict()
