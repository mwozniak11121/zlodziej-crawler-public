from pathlib import Path

import pytest

from zlodziej_crawler import paths


@pytest.fixture(autouse=True)
def mock_pathlib(monkeypatch):
    def pass_function():
        pass

    RESULTS_PATH = Path("/testdir/results")

    monkeypatch.setattr(Path, "mkdir", lambda *args, **kwargs: pass_function())
    monkeypatch.setattr(Path, "touch", lambda *args, **kwargs: pass_function())
    monkeypatch.setattr(paths, "RESULTS_PATH", RESULTS_PATH)


def test_results_path():
    assert paths.RESULTS_PATH == Path("/testdir/results")


def test_get_category_path():
    path = paths.get_category_path("wynajem/mieszkanie/wroclaw")
    assert path == Path("/testdir/results/wynajem/mieszkanie/wroclaw")
