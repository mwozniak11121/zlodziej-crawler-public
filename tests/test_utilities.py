import pytest

from zlodziej_crawler.models import BuildingType, OfferType, Website
from zlodziej_crawler.utilities import (
    translate_to_enum,
    translate_city,
    translate_months,
)


@pytest.mark.parametrize(
    "value, enum_class, expected",
    [
        ("olx.pl", Website, Website.OLX),
        ("PRIVATE", OfferType, OfferType.PRIVATE),
        ("BlOk", BuildingType, BuildingType.BLOK),
        ("DOM WOLNOSTOJĄCY", BuildingType, BuildingType.DOM_WOLNOSTAJACY),
    ],
)
def test_translate_to_enum(value, enum_class, expected):
    assert translate_to_enum(value, enum_class) == expected


@pytest.mark.parametrize(
    "city, expected",
    [
        ("Wrocław", "wroclaw"),
        ("Zielona Góra", "zielonagora"),
        ("zielona Gora", "zielonagora"),
    ],
)
def test_translate_city(city, expected):
    assert translate_city(city) == expected


@pytest.mark.parametrize(
    "content, expected",
    [
        ("STYCZEń", "january"),
        ("maja", "may"),
        ("  GRUDZIEń  ", "december"),
    ],
)
def test_translate_months(content, expected):
    assert translate_months(content) == expected
